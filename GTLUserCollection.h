/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2016 Google Inc.
 */

//
//  GTLUserCollection.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   user/v1
// Description:
//   This should be for movie quotes but it says User
// Classes:
//   GTLUserCollection (0 custom class methods, 2 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

@class GTLUserUser;

// ----------------------------------------------------------------------------
//
//   GTLUserCollection
//

// This class supports NSFastEnumeration over its "items" property. It also
// supports -itemAtIndex: to retrieve individual objects from "items".

@interface GTLUserCollection : GTLCollectionObject
@property (nonatomic, retain) NSArray *items;  // of GTLUserUser
@property (nonatomic, copy) NSString *nextPageToken;
@end
