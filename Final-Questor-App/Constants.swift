//
//  Constants.swift
//  Final-Questor-App
//
//  Created by Asad Khan on 02/07/2016.
//  Copyright © 2016 Adrian Humphrey. All rights reserved.
//

import Foundation


struct Constants{
    static let kBaseURL = "https://final-questor-app.appspot.com/"
    static var tapCount:Int = 0
    static var lat: Double = 0
    static var lon: Double = 0
    static var profileGifPathArray = [String]()
    static var locationOn = false
    static var notificationOn = false
    
    
}

